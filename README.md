## Table of Contents

- [About](#about)
- [Features](#features)
- [Installation](#installation)

## About

An android app which can be used to upload files like images and PDFs using Retrofit

## Features

- Feature 1: Upload multipe files
- Feature 2: Upload files of a certain size only
- Feature 3: Show the type of file using icons
- Feature 4: Show the file name

## Installation

Follow these simple steps:

1. Clone the repository.
2. Run the application in Android Studio.


