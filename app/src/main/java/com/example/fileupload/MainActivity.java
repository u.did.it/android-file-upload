package com.example.fileupload;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity {

    private static final int FILE_PICKER_REQUEST_CODE_1 = 1;
    private static final int FILE_PICKER_REQUEST_CODE_2 = 2;

    private LinearLayout selectFileLayout1, selectFileLayout2;
    private Button submitButton;
    private Uri selectedFileUri;

    private RequestQueue requestQueue;

    private ApiService apiService;

    Uri fileUri1 = null;
    Uri fileUri2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upload.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService = retrofit.create(ApiService.class);

        selectFileLayout1 = findViewById(R.id.selectFileLayout1);
        selectFileLayout2 = findViewById(R.id.selectFileLayout2);
        submitButton = findViewById(R.id.submitButton);

        requestQueue = Volley.newRequestQueue(this);

        selectFileLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilePicker(FILE_PICKER_REQUEST_CODE_1);
            }
        });

        selectFileLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilePicker(FILE_PICKER_REQUEST_CODE_2);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Implement file upload using Volley
                uploadFiles(fileUri1, fileUri2,"Aftab");

                //Log.e("UJULOG", "onClick: "+fileUri.toString() );
            }
        });
    }

    private void openFilePicker(int FILE_PICKER_REQUEST_CODE) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, FILE_PICKER_REQUEST_CODE);
    }

    private void uploadFiles(Uri fileUri1, Uri fileUri2, String additionalString) {
        MultipartBody.Part body1 = null;
        if (fileUri1 != null) {
            File file1 = new File(getRealPathFromUri(this, fileUri1));
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
            body1 = MultipartBody.Part.createFormData("file1", file1.getName(), requestFile1);
        }

        MultipartBody.Part body2 = null;
        if (fileUri2 != null) {
            File file2 = new File(getRealPathFromUri(this, fileUri2));
            RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
            body2 = MultipartBody.Part.createFormData("file2", file2.getName(), requestFile2);
        }

        // Create a RequestBody for the additional string
        RequestBody additionalStringBody = RequestBody.create(MediaType.parse("multipart/form-data"), additionalString);

        // Finally, execute the request
        Call<ResponseBody> call = apiService.uploadFiles(body1, body2, additionalStringBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    ResponseBody responseBody = response.body();
                    if (responseBody != null) {
                        try {
                            String responseBodyString = responseBody.string();
                            Log.e("UJULOG", "Response Body: " + responseBodyString);
                        } catch (IOException e) {
                            e.printStackTrace();
                            // Handle error reading response body
                        }
                    } else {
                        Log.e("UJULOG", "Response Body is null");
                    }
                } else {
                    // Handle unsuccessful response
                    Log.e("UJULOG", "Error uploading files. HTTP status code: " + response.code());
                    // You can also log the error body if needed
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                // Handle failure
                Log.e("UJULOG", "Error uploading files: " + t.getMessage());
                Toast.makeText(MainActivity.this, "Error uploading files: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



    public String getRealPathFromUri(Context context, Uri uri) {
        String filePath = null;
        if (uri == null) {
            return null;
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            InputStream inputStream = null;
            try {
                inputStream = context.getContentResolver().openInputStream(uri);
                if (inputStream != null) {
                    // Get the original file name from the content resolver
                    String originalFileName = getOriginalFileName(context, uri);
                    // Create a temporary file with the appropriate file extension
                    File tempFile = createTempFile(context, originalFileName);
                    if (tempFile != null) {
                        FileOutputStream outputStream = new FileOutputStream(tempFile);
                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }
                        filePath = tempFile.getAbsolutePath();
                        outputStream.close();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            filePath = uri.getPath();
        }
        return filePath;
    }

    private String getOriginalFileName(Context context, Uri uri) {
        String[] projection = {MediaStore.Images.Media.DISPLAY_NAME};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
                return cursor.getString(columnIndex);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private File createTempFile(Context context, String originalFileName) {
        try {
            String fileExtension = getFileExtension(originalFileName);
            return File.createTempFile("temp", fileExtension, context.getCacheDir());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getFileExtension(String fileName) {
        if (fileName != null && !fileName.isEmpty()) {
            int lastDotIndex = fileName.lastIndexOf(".");
            if (lastDotIndex != -1) {
                return fileName.substring(lastDotIndex);
            }
        }
        // Default to ".tmp" if file extension cannot be determined
        return ".tmp";
    }

    private String getFileNameFromUri(Uri uri) {
        String fileName = null;
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            if (nameIndex != -1) {
                fileName = cursor.getString(nameIndex);
            }
            cursor.close();
        }
        return fileName;
    }

    private long getFileSize(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
            if (sizeIndex != -1) {
                return cursor.getLong(sizeIndex);
            }
            cursor.close();
        }
        return 0;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_PICKER_REQUEST_CODE_1 && resultCode == RESULT_OK) {
            handleFilePickerResult(data, 1);
        } else if (requestCode == FILE_PICKER_REQUEST_CODE_2 && resultCode == RESULT_OK) {
            handleFilePickerResult(data, 2);
        }
    }

    private void handleFilePickerResult(Intent data, int pickerNumber) {
        if (data != null) {
            Uri uri = data.getData();
            if (uri != null) {
                if (pickerNumber == 1) {
                    fileUri1 = uri; // Assign the selected file URI to fileUri1
                } else if (pickerNumber == 2) {
                    fileUri2 = uri; // Assign the selected file URI to fileUri2
                }

                // Get file type
                String fileType = getContentResolver().getType(uri);
                if (fileType != null) {
                    if (fileType.startsWith("image/") || fileType.equals("application/pdf")) {
                        // Get file size
                        long fileSize = getFileSize(uri);
                        // Convert fileSize to MB
                        long fileSizeInMB = fileSize / (1024 * 1024);
                        if (fileSizeInMB > 2) {
                            // File size exceeds 2MB, show error message
                            Toast.makeText(this, "File size exceeds 2MB limit", Toast.LENGTH_SHORT).show();
                            // Clear the selected file URI
                            if (pickerNumber == 1) {
                                fileUri1 = null;
                            } else if (pickerNumber == 2) {
                                fileUri2 = null;
                            }
                            return;
                        }

                        // It's an image or a PDF
                        ImageView iconView = new ImageView(this);
                        TextView fileNameView = new TextView(this);
                        LinearLayout fileLayout;
                        if (pickerNumber == 1) {
                            fileLayout = selectFileLayout1;
                        } else {
                            fileLayout = selectFileLayout2;
                        }
                        iconView.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        ));
                        fileNameView.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        ));
                        if (fileType.startsWith("image/")) {
                            iconView.setImageResource(R.drawable.ic_image_black);
                        } else {
                            iconView.setImageResource(R.drawable.ic_pdf_black);
                        }
                        // Get file name
                        String fileName = getFileNameFromUri(uri);
                        fileNameView.setText(fileName);
                        fileLayout.removeAllViews();
                        fileLayout.addView(iconView);
                        fileLayout.addView(fileNameView);
                    } else {
                        Toast.makeText(this, "Unsupported file type", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

}
