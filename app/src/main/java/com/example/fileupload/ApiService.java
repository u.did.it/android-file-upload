package com.example.fileupload;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {
    @Multipart
    @POST("upload")
    Call<Void> uploadFile(
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST("your/upload/endpoint")
    Call<ResponseBody> uploadFiles(
            @Part MultipartBody.Part file1,
            @Part MultipartBody.Part file2,
            @Part("additional_string") RequestBody additionalString
    );

}
